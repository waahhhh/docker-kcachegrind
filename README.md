# docker-KCacheGrind

see: http://kcachegrind.sourceforge.net/html/Home.html

## running the KCacheGrind

At first Docker must be allowed to connect to the X-Server:
```bash
xhost + local:docker
```

```bash
docker-compose run kcachegrind
```
