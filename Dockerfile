FROM ubuntu:latest

RUN apt-get update -q  \
    && apt-get install -q -y kcachegrind \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

CMD kcachegrind
